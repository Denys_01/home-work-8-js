let pElements = document.getElementsByTagName('p');
for( let value of pElements){
    value.style.backgroundColor = '#ff0000';
}

let element = document.getElementById("optionsList");
console.log(element);

let element1 = element.parentElement;
console.log(element1);

let listNodes = element.childNodes;
for(let value of listNodes){
    console.log(value ,typeof(value))
}

let newP = document.createElement('p');
let testP = document.getElementById('testParagraph');
testP.append(newP);
newP.innerHTML = 'This is a paragraph';

let mainHeader = document.getElementsByClassName('main-header');
let elementsAll = mainHeader[0].childNodes ;
console.log(elementsAll);


let sectionTitleElements = document.getElementsByClassName('section-title');
for( let value of sectionTitleElements){
    value.classList.remove('section-title');
}